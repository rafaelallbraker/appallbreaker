﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

public class AppManager : MonoBehaviour 
{
	[Header("UI")]
	public List<GameObject> menuPanels = new List<GameObject>();
	GameObject mainCanvas;

	// Use this for initialization
	void Start () 
	{
		videoPlayer = GameObject.Find("Video Player").GetComponent<VideoPlayer>();		
		mainCanvas = GameObject.Find("MainCanvas");
		videoPlayerCanvas = GameObject.Find("VideoCanvas");

		videoPlayerCanvas.SetActive(false);
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(!Input.GetMouseButton(0))
		{
			if(videoPlayer.clip && seekSlider.maxValue == videoPlayer.clip.frameCount)
			{
				seekSlider.value = videoPlayer.frame;
			}
		}
		else
		{
			if(videoPlayer.clip && seekSlider.maxValue == videoPlayer.clip.frameCount)
			{
				videoPlayer.frame = (long)seekSlider.value;
			}
		}	
	}

	#region UI Management

	public void JumpToPanel(int panelIndex)
	{
		foreach(GameObject g in menuPanels)
		{
			g.SetActive(false);

			if(g == menuPanels[panelIndex])
			{
				g.SetActive(true);
			}
		}
	}

	#endregion

	#region Videos

	[Header("Videos")]
	public List<string> videoURLs = new List<string>();
	public List<VideoClip> videoClips = new List<VideoClip>();
	VideoPlayer videoPlayer;

	[Header("VideoPlayer UI")]
	public UnityEngine.UI.Image playButton;
	public List<Sprite> playbackSprites = new List<Sprite>();
	public UnityEngine.UI.Slider seekSlider;
	GameObject videoPlayerCanvas;

	public void PlayVideo(VideoClip clip)
	{
		videoPlayer.source = VideoSource.VideoClip;
		videoPlayer.clip = clip;

		mainCanvas.SetActive(false);
		videoPlayerCanvas.SetActive(true);
		videoPlayer.Play();
		playButton.sprite = playbackSprites[1];

		playButton.gameObject.GetComponent<UnityEngine.UI.Button>().onClick.RemoveAllListeners();
		playButton.gameObject.GetComponent<UnityEngine.UI.Button>().onClick.AddListener(()=>{PauseVideo();});

		AdjustPlayer();
	}
	public void PlayVideo(string url)
	{
		videoPlayer.source = VideoSource.Url;
		videoPlayer.url = url;

		mainCanvas.SetActive(false);
		videoPlayerCanvas.SetActive(true);
		videoPlayer.Play();
		playButton.sprite = playbackSprites[1];

		playButton.gameObject.GetComponent<UnityEngine.UI.Button>().onClick.RemoveAllListeners();
		playButton.gameObject.GetComponent<UnityEngine.UI.Button>().onClick.AddListener(()=>{PauseVideo();});

		AdjustPlayer();
	}

	public void StopVideo()
	{
		mainCanvas.SetActive(true);
		videoPlayerCanvas.SetActive(false);
		videoPlayer.Stop();
		playButton.sprite = playbackSprites[0];
	}

	public void PauseVideo()
	{
		videoPlayer.Pause();
		playButton.sprite = playbackSprites[0];

		playButton.gameObject.GetComponent<UnityEngine.UI.Button>().onClick.RemoveAllListeners();
		playButton.gameObject.GetComponent<UnityEngine.UI.Button>().onClick.AddListener(()=>{ResumeVideo();});
	}

	public void ResumeVideo()
	{
		videoPlayer.Play();
		playButton.sprite = playbackSprites[1];

		playButton.gameObject.GetComponent<UnityEngine.UI.Button>().onClick.RemoveAllListeners();
		playButton.gameObject.GetComponent<UnityEngine.UI.Button>().onClick.AddListener(()=>{PauseVideo();});
	}

	public void SeekVideo()
	{
		videoPlayer.frame = (long)seekSlider.value;
	}

	public void AdjustPlayer()
	{
		seekSlider.maxValue = videoPlayer.clip.frameCount;
	}

	#endregion

	#region Experiences

	[Header("Experiences")]
	bool temp;

	public void LoadExperience(int sceneIndex)
	{
		UnityEngine.SceneManagement.SceneManager.LoadScene(sceneIndex);
	}

	#endregion

	#region Demos

	public void LaunchDemo(string bundleId)
	{
		bool fail = false;
		AndroidJavaClass up = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
		AndroidJavaObject ca = up.GetStatic<AndroidJavaObject>("currentActivity");
		AndroidJavaObject packageManager = ca.Call<AndroidJavaObject>("getPackageManager");
	
		AndroidJavaObject launchIntent = null;
		try
		{
			launchIntent = packageManager.Call<AndroidJavaObject>("getLaunchIntentForPackage",bundleId);
		}
		catch (System.Exception e)
		{
			Debug.LogException(e);
			fail = true;
		}

		if (fail)
		{ //open app in store
			Application.OpenURL("https://google.com");
		}
		else //open the app
			ca.Call("startActivity",launchIntent);

		up.Dispose();
		ca.Dispose();
		packageManager.Dispose();
		launchIntent.Dispose();
	}

	#endregion
}
