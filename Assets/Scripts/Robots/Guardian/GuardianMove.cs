﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GuardianMove : MonoBehaviour 
{
	Transform targetTransform;
	Vector3 oldEulerAngles;

	[HideInInspector]
	public Animator animGuardian;

    [HideInInspector]
	public bool canLookAt = true, isRotating;

	// Use this for initialization
	void Start () 
	{
		animGuardian = GetComponent<Animator>();

		if(!targetTransform)
		{
			targetTransform = GameObject.Find("First Person Camera").transform;
		}

        GameManager.instance.CountdownStart(10);
        oldEulerAngles = transform.rotation.eulerAngles;
    }

    // Update is called once per frame
    void Update () 
	{
        if (!GameManager.instance.gameOver)
        {
            if (canLookAt)
                LookAtTarget();

            //

            // CheckRotation();
        }
	}
	//
	void LookAtTarget()
	{
		 Vector3 targetPostition = new Vector3( targetTransform.position.x, 
                                        transform.position.y, 
                                        targetTransform.position.z ) ;
		//
 		transform.LookAt( targetPostition ) ;
	}

	public void ToggleRotation(bool rotate)
	{
		canLookAt = rotate;
	}

	void CheckRotation()
	{
		if (oldEulerAngles.y == transform.rotation.eulerAngles.y)
		{
           animGuardian.SetBool("RotatingR", false);
		   animGuardian.SetBool("RotatingL", false);
		} 
		else
		{
			if(oldEulerAngles.y + transform.rotation.eulerAngles.y > 0)
			{
				animGuardian.SetBool("RotatingL", true);
			}
			else
			{
				animGuardian.SetBool("RotatingR", true);
			}
			oldEulerAngles = transform.rotation.eulerAngles;
		}	
	}
}
