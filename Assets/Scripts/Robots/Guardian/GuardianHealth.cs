﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GuardianHealth : MonoBehaviour 
{
	[Header("FX")]
	public GameObject prefabDead;
	public GameObject prefabHeavy;

	[Header("Health")]
	public float initHealth = 10000;
	float totalHealth = 10000;

	float heavyDamage = 1000;
	float heavyHelper = 0;

	bool invulnerable = false;
    [HideInInspector]
	public bool isDead = false, isHurt = false;
    
    [Header("UI")]

	UnityEngine.UI.Slider sliderHealth;

	GuardianMove guardianMove;
    GuardianShoot guardianShoot;

	// Use this for initialization
	void Start () 
	{
		GameObject.Find("First Person Camera").GetComponent<CameraShoot>().AssignGuardian(gameObject);
		guardianMove = GetComponent<GuardianMove>();
        guardianShoot = GetComponent<GuardianShoot>();

        sliderHealth = GameObject.Find("bosshealth").GetComponent<UnityEngine.UI.Slider>();
        sliderHealth.gameObject.SetActive(true);
		sliderHealth.maxValue = initHealth;

		totalHealth = initHealth;
		StartCoroutine(TemporalInvulnerablility(10));
	}

	private void Update() 
	{
        if (!GameManager.instance.gameOver)
        {
            sliderHealth.value = totalHealth;

            if (!isDead)
            {
                if (heavyHelper >= heavyDamage && !isDead && !isHurt)
                {
                    isHurt = true;
                    guardianMove.animGuardian.SetBool("GetHurt", true);
                    Instantiate(prefabHeavy, transform.position, Quaternion.identity);
                    //StartCoroutine(TemporalInvulnerablility(2));
                    heavyHelper = 0;
                }
                else if (!isDead)
                {
                    guardianMove.animGuardian.SetBool("GetHurt", false);
                }
            }
            else
            {
                guardianMove.animGuardian.SetBool("Drones", false);
                heavyHelper = 0;
            }
        }
	}

	public void TakeDamage(float quantity)
	{
		if(!invulnerable)
		{
			totalHealth -= quantity;

            if(guardianShoot.actualMode == ActualMode.ShootMode)
			    heavyHelper += quantity;
		}

        if(totalHealth <= initHealth/2)
        {
            guardianShoot.actualMode = ActualMode.DroneMode;
            guardianShoot.canDrone = true;
        }

		if(totalHealth <= 0 && !isDead)
		{
			isDead = true;
            GameManager.instance.GameOver(true);
            sliderHealth.gameObject.SetActive(false);
            Instantiate(prefabDead, transform.position, Quaternion.identity);
		}
	}

	public IEnumerator TemporalInvulnerablility(float time)
	{
		invulnerable = true;
        guardianShoot.canShoot = false;

        yield return new WaitForSeconds(time);

        if (!GameManager.instance.gameOver)
        {
            invulnerable = false;

            if (guardianShoot.actualMode == ActualMode.ShootMode)
                guardianShoot.canShoot = true;

            isHurt = false;
        }
    }
}

public enum ActualMode
{
    ShootMode,
    DroneMode
}
