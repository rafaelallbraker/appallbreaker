﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GuardianShoot : MonoBehaviour
{
    [Header("Mode")]
    public ActualMode actualMode = ActualMode.ShootMode;

    Animator animGuardian;
    GuardianMove guardianMove;
    GuardianHealth guardianHealth;

    [Header("Shooting")]
    public float timeToShoot = 10;
    float timeHelper = 0;

    public Transform shotSpawnpoint;
    public GameObject bulletPrefab;

    [Header("Drones")]
    public GameObject dronePrefab;
    public List<Transform> droneSpawnpoints = new List<Transform>();

    [HideInInspector]
    public bool canShoot = false, canDrone = false;

    // Use this for initialization
    void Start ()
    {
        timeHelper = timeToShoot;

        animGuardian = GetComponent<Animator>();
        guardianMove = GetComponent<GuardianMove>();
        guardianHealth = GetComponent<GuardianHealth>();
    }
	
	// Update is called once per frame
	void Update ()
    {
        if (!GameManager.instance.gameOver)
        {
            if (actualMode == ActualMode.ShootMode)
            {
                if (canShoot)
                {
                    if (timeHelper > 0)
                    {
                        timeHelper -= Time.deltaTime;
                    }
                    else
                    {
                        Shoot();
                        timeHelper = timeToShoot;
                    }
                }
            }
            else
            {
                canShoot = false;

                if (!guardianHealth.isDead)
                {
                    animGuardian.SetBool("Drones", true);
                    if (canDrone)
                    {
                        if (timeHelper > 0)
                        {
                            timeHelper -= Time.deltaTime;
                        }
                        else
                        {
                            canDrone = false;
                            Drone();
                            timeHelper = timeToShoot;
                        }
                    }
                }
                else
                    animGuardian.SetBool("Drones", false);
            }
        }
        else
        {
            animGuardian.SetBool("Drones", false);
            animGuardian.SetBool("GetHurt", true);
        }
	}

    public void Shoot()
    {
        animGuardian.SetBool("Shoot", true);
        //
        StartCoroutine(ShootCoroutine());
    }

    public IEnumerator ShootCoroutine()
    {
        guardianMove.canLookAt = false;

        yield return new WaitForSeconds(1);

        Instantiate(bulletPrefab, shotSpawnpoint.position, Quaternion.identity);

        yield return new WaitForSeconds(1.5f);

        animGuardian.SetBool("Shoot", false);
        guardianMove.canLookAt = true;
    }

    public void Drone()
    {
        guardianMove.canLookAt = false;
        //
        StartCoroutine(DroneCoroutine());
    }

    public IEnumerator DroneCoroutine()
    {
        guardianMove.canLookAt = false;

        yield return new WaitForSeconds(1);

        Instantiate(dronePrefab, droneSpawnpoints[0].position, Quaternion.identity);

        yield return new WaitForSeconds(1);

        Instantiate(dronePrefab, droneSpawnpoints[1].position, Quaternion.identity);

        yield return new WaitForSeconds(1);

        canDrone = true;
    }
}
