﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour 
{
	[Header("UI")]
	public GameObject gameUI;
    public GameObject gameOverUI;
    public UnityEngine.UI.Text readyUI;

    [Header("Game")]
    public bool gameOver = false;

    public static GameManager instance;

	// Use this for initialization
	void Start ()
    {
        if(!instance)
        {
            instance = this;
        }
        gameOverUI.SetActive(false);
        readyUI.gameObject.SetActive(false);
    }

	public void PauseGame(bool pause)
	{
		if(pause)
			Time.timeScale = 0;
		else
			Time.timeScale = 1;
	}

    public void GameOver(bool win)
    {
        gameOver = true;

        if(win)
        {
            gameOverUI.GetComponent<UnityEngine.UI.Text>().text += "\n You Win";
        }
        else
        {
            gameOverUI.GetComponent<UnityEngine.UI.Text>().text += "\n You Lose";
        }

        gameOverUI.SetActive(true);
    }

	public void CountdownStart(int countTime)
	{
		StartCoroutine(CountdownStartCoroutine(countTime));
	}

	public IEnumerator CountdownStartCoroutine(int countTime)
    {
        readyUI.gameObject.SetActive(true);
        readyUI.text = countTime.ToString();

		while(countTime > 0)
		{
			countTime--;
			yield return new WaitForSecondsRealtime(1);
            readyUI.text = Mathf.FloorToInt(countTime).ToString();
		}

        StartCoroutine(CoroutineStart());
		//
		if(gameUI)
		{
			gameUI.SetActive(true);
		}
		else
		{
			Debug.LogWarning("There is no UI panel assigned");
		}
	}

    public IEnumerator CoroutineStart()
    {
        readyUI.text = "Start!";

        yield return new WaitForSeconds(2);

        readyUI.gameObject.SetActive(false);
    }

	public void RestartGame()
	{
		UnityEngine.SceneManagement.SceneManager.LoadScene(UnityEngine.SceneManagement.SceneManager.GetActiveScene().buildIndex);
	}
}
