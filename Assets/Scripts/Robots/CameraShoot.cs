﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraShoot : MonoBehaviour 
{
	public GameObject explosionPrefab, laserLine;

	[Header("Robot")]
	GuardianHealth guardianHealth;

	[Header("Firing")]
	float fireDamage = 100;
	public Transform bulletSpawn;
	bool isShooting = false, allowfire = true; 
	public float fireRate = 0.5f;
 	private float lastShot = 0.0f;

    [Header("Audio")]
    public AudioSource shootSource;

	// Use this for initialization
	void Start () 
	{
		laserLine.SetActive(false);
	}

	public void AssignGuardian(GameObject guardianGO)
	{
		guardianHealth = guardianGO.GetComponent<GuardianHealth>();
	}

	void FixedUpdate() 
	{
		if(isShooting && allowfire)
		{
			Fire();
		}	
	}

	public void Shoot(bool shooting)
	{
		isShooting = shooting;
	}

	public void Fire()
	{
        if (!GameManager.instance.gameOver)
        {
            RaycastHit hit;

            if (Time.time > fireRate + lastShot)
            {
                if (Physics.Raycast(transform.position, transform.forward, out hit))
                {
                    if (hit.transform.tag == "Robot")
                    {
                        Instantiate(explosionPrefab, hit.point, Quaternion.identity);
                        float tempDamage = fireDamage * (Vector3.Distance(transform.position, guardianHealth.transform.position) * .15f);
                        guardianHealth.TakeDamage(tempDamage);
                    }
                    else if (hit.transform.tag == "Drone")
                    {
                        hit.transform.GetComponent<Drone>().Die();
                    }
                }
                //
                lastShot = Time.time;
                StopCoroutine(ShowLaser());
                StartCoroutine(ShowLaser());
            }
        }
	}

	public IEnumerator ShowLaser()
	{
		laserLine.SetActive(true);

        shootSource.Stop();
        shootSource.Play();

        yield return new WaitForSecondsRealtime(0.05f);

		laserLine.SetActive(false);
	}
}
