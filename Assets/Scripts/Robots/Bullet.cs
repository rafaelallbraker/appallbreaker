﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour {

	public float bulletSpeed = 2;
	public GameObject prefabExplosion;
    Transform target;

	// Use this for initialization
	void Start () 
	{
		StartCoroutine(DelayDie());
        target = GameObject.Find("First Person Camera").transform;
    }
	
	// Update is called once per frame
	void Update ()
    {
        transform.position = Vector3.MoveTowards(transform.position, target.position, Time.deltaTime * bulletSpeed);
	}

	public IEnumerator DelayDie()
	{
		yield return new WaitForSecondsRealtime(3);
		Die();
	}

	public void Die()
	{
		Instantiate(prefabExplosion,transform.position, Quaternion.identity);
		Destroy(gameObject);
	}

	/// <summary>
	/// OnCollisionEnter is called when this collider/rigidbody has begun
	/// touching another rigidbody/collider.
	/// </summary>
	/// <param name="other">The Collision data associated with this collision.</param>
	void OnCollisionEnter(Collision other)
	{
		if(other.transform.tag == "Player")
		{
            other.transform.GetComponent<PlayerHealth>().TakeDamage();
		}
		
		Die();
	}
}
