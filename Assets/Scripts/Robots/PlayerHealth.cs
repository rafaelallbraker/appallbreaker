﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHealth : MonoBehaviour
{
    bool isProtected = false, isDead = false;

    public int totalLives = 4;
    int sessionLives = 0;

    public UnityEngine.UI.Text txtLives;

	// Use this for initialization
	void Start ()
    {
        sessionLives = totalLives;
	}

    private void Update()
    {
        txtLives.text = "x" + sessionLives;
    }

    public void TakeDamage()
    {
        if (!isProtected)
        {
            if(!isDead)
                sessionLives--;
        }

        if(sessionLives <= 0)
        {
            GameManager.instance.GameOver(false);
            isDead = true;
        }
    }

    public void Protect(bool shielded)
    {
        isProtected = shielded;
    }
}
