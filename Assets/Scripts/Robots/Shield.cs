﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shield : MonoBehaviour 
{
	public Animator animShield;
	// Use this for initialization
	void Start () 
	{
		if(!animShield)
		{
			animShield = GetComponent<Animator>();
		}		
	}

	public void Protect(bool on)
	{
		animShield.SetBool("Protect", on);
	}
}
