﻿using System.Collections.Generic;
using GoogleARCore;
using UnityEngine;
using UnityEngine.Rendering;

/// <summary>
/// Controls the HelloAR example.
/// </summary>
public class ARCoreManager : MonoBehaviour
{
	public static ARCoreManager instance;

	[Header("ARCore Required")]
	/// <summary>
	/// The first-person camera being used to render the passthrough camera image (i.e. AR background).
	/// </summary>
	public Camera FirstPersonCamera;

	/// <summary>
	/// A prefab for tracking and visualizing detected planes.
	/// </summary>
	public GameObject TrackedPlanePrefab;
	List<GameObject> planeObjects = new List<GameObject>();

	[Header("Spawning")]

	/// <summary>
	/// A model to place when a raycast from a user touch hits a plane.
	/// </summary>
	public GameObject DemoPrefab;
	public float yRotationOffset = 10f;
	[HideInInspector]
	public GameObject instancedPrefab = null;
    public List<GameObject> goToActivate = new List<GameObject>();

	[Header("UI")]

	/// <summary>
	/// A gameobject parenting UI for displaying the "searching for planes" snackbar.
	/// </summary>
	public GameObject SearchingForPlaneUI;
	public GameObject resetButton;

	[Header("Tracking Planes")]

	/// <summary>
	/// A list to hold new planes ARCore began tracking in the current frame. This object is used across
	/// the application to avoid per-frame allocations.
	/// </summary>
	private List<TrackedPlane> m_NewPlanes = new List<TrackedPlane>();

	/// <summary>
	/// A list to hold all planes ARCore is tracking in the current frame. This object is used across
	/// the application to avoid per-frame allocations.
	/// </summary>
	private List<TrackedPlane> m_AllPlanes = new List<TrackedPlane>();

	/// <summary>
	/// True if the app is in the process of quitting due to an ARCore connection error, otherwise false.
	/// </summary>
	private bool m_IsQuitting = false;

	private void Start() 
	{
		if(!ARCoreManager.instance)
		{
			ARCoreManager.instance = this;
		}

		if(resetButton)
			resetButton.SetActive(false);

        foreach (GameObject g in goToActivate)
        {
            g.SetActive(false);
        }
    }

	/// <summary>
	/// The Unity Update() method.
	/// </summary>
	public void Update()
	{
		if (Input.GetKey(KeyCode.Escape))
		{
			GetBack();
		}

		_QuitOnConnectionErrors();

		// Check that motion tracking is tracking.
		if (Frame.TrackingState != TrackingState.Tracking)
		{
			const int lostTrackingSleepTimeout = 15;
			Screen.sleepTimeout = lostTrackingSleepTimeout;
			return;
		}

		Screen.sleepTimeout = SleepTimeout.NeverSleep;

		// Iterate over planes found in this frame and instantiate corresponding GameObjects to visualize them.
		Frame.GetPlanes(m_NewPlanes, TrackableQueryFilter.New);
		for (int i = 0; i < m_NewPlanes.Count; i++)
		{
			// Instantiate a plane visualization prefab and set it to track the new plane. The transform is set to
			// the origin with an identity rotation since the mesh for our prefab is updated in Unity World
			// coordinates.
			GameObject planeObject = Instantiate(TrackedPlanePrefab, Vector3.zero, Quaternion.identity, transform);
			planeObject.GetComponent<GoogleARCore.HelloAR.TrackedPlaneVisualizer>().Initialize(m_NewPlanes[i]);
			planeObjects.Add(planeObject);
		}

		// Disable the snackbar UI when no planes are valid.
		Frame.GetPlanes(m_AllPlanes);
		bool showSearchingUI = true;
		for (int i = 0; i < m_AllPlanes.Count; i++)
		{
			if (m_AllPlanes[i].TrackingState == TrackingState.Tracking)
			{
				showSearchingUI = false;
				break;
			}
		}

		SearchingForPlaneUI.SetActive(showSearchingUI);

		// If the player has not touched the screen, we are done with this update.
		Touch touch;
		if (Input.touchCount < 1 || (touch = Input.GetTouch(0)).phase != TouchPhase.Began)
		{
			return;
		}

		// Raycast against the location the player touched to search for planes.
		TrackableHit hit;
		TrackableHitFlags raycastFilter = TrackableHitFlags.PlaneWithinBounds | TrackableHitFlags.PlaneWithinPolygon;

		if (Session.Raycast(touch.position.x, touch.position.y, raycastFilter, out hit) && !instancedPrefab)
        {
            foreach (GameObject g in goToActivate)
            {
                g.SetActive(true);
            }

            instancedPrefab = Instantiate(DemoPrefab, hit.Pose.position, hit.Pose.rotation);

			// Create an anchor to allow ARCore to track the hitpoint as understanding of the physical
			// world evolves.
			var anchor = hit.Trackable.CreateAnchor(hit.Pose);

			// Andy should look at the camera but still be flush with the plane.
			instancedPrefab.transform.LookAt(FirstPersonCamera.transform);
			instancedPrefab.transform.rotation = Quaternion.Euler(0.0f,
				instancedPrefab.transform.rotation.eulerAngles.y + yRotationOffset, instancedPrefab.transform.rotation.z);

			// Make Andy model a child of the anchor.
			instancedPrefab.transform.parent = anchor.transform;
		}
		else if(instancedPrefab)
		{
			if(resetButton)
				resetButton.SetActive(true);

			foreach(GameObject g in planeObjects)
			{
				g.SetActive(false);
			}
		}
	}

	/// <summary>
	/// Quit the application if there was a connection error for the ARCore session.
	/// </summary>
	private void _QuitOnConnectionErrors()
	{
		if (m_IsQuitting)
		{
			return;
		}

		// Quit if ARCore was unable to connect and give Unity some time for the toast to appear.
		if (Session.ConnectionState == SessionConnectionState.UserRejectedNeededPermission)
		{
			_ShowAndroidToastMessage("Camera permission is needed to run this application.");
			m_IsQuitting = true;
			Invoke("DoQuit", 0.5f);
		}
		else if (Session.ConnectionState == SessionConnectionState.ConnectToServiceFailed)
		{
			_ShowAndroidToastMessage("ARCore encountered a problem connecting.  Please start the app again.");
			m_IsQuitting = true;
			Invoke("DoQuit", 0.5f);
		}
	}

	/// <summary>
	/// Actually quit the application.
	/// </summary>
	private void DoQuit()
	{
		Application.Quit();
	}

	/// <summary>
	/// Show an Android toast message.
	/// </summary>
	/// <param name="message">Message string to show in the toast.</param>
	private void _ShowAndroidToastMessage(string message)
	{
		AndroidJavaClass unityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
		AndroidJavaObject unityActivity = unityPlayer.GetStatic<AndroidJavaObject>("currentActivity");

		if (unityActivity != null)
		{
			AndroidJavaClass toastClass = new AndroidJavaClass("android.widget.Toast");
			unityActivity.Call("runOnUiThread", new AndroidJavaRunnable(() =>
			{
				AndroidJavaObject toastObject = toastClass.CallStatic<AndroidJavaObject>("makeText", unityActivity,
					message, 0);
				toastObject.Call("show");
			}));
		}
	}

	public void ResetInstance()
	{
		if(instancedPrefab)
		{
			Destroy(instancedPrefab);
			instancedPrefab = null;
		}
	}

	public void GetBack()
	{
		ResetInstance();
		UnityEngine.SceneManagement.SceneManager.LoadScene(0);
	}
}