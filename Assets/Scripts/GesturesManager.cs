﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DigitalRubyShared;

public class GesturesManager : MonoBehaviour 
{
	public ScaleGestureRecognizer scaleGesture;
	public RotateGestureRecognizer rotateGesture;
	GameObject ArPrefab;
	Vector3 initScale = Vector3.one;

	// Use this for initialization
	void Start () 
	{
		scaleGesture = new ScaleGestureRecognizer
		{
			ZoomSpeed = 6.0f // for a touch screen you'd probably not do this, but if you are using ctrl + mouse wheel then this helps zoom faster
		};
		scaleGesture.StateUpdated += Gesture_Updated;
		FingersScript.Instance.AddGesture(scaleGesture);

		rotateGesture = new RotateGestureRecognizer();
		rotateGesture.StateUpdated += RotateGestureCallback;
		FingersScript.Instance.AddGesture(rotateGesture);

		// the scale and pan can happen together
		scaleGesture.AllowSimultaneousExecution(rotateGesture);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	private void Gesture_Updated(GestureRecognizer gesture)
	{
		if (scaleGesture.State != GestureRecognizerState.Executing || scaleGesture.ScaleMultiplier == 1.0f)
		{
			return;
		}

		// invert the scale so that smaller scales actually zoom out and larger scales zoom in
		float scale = 1.0f - (1.0f - scaleGesture.ScaleMultiplier);

		//TODO Scale instanced AR object with gesture
		if(ARCoreManager.instance.instancedPrefab)
		{
			initScale = ARCoreManager.instance.instancedPrefab.transform.localScale;
			ARCoreManager.instance.instancedPrefab.transform.localScale = Vector3.ClampMagnitude((initScale*scale), 4);
		}
	}

	private void RotateGestureCallback(GestureRecognizer gesture)
	{
		if (gesture.State == GestureRecognizerState.Executing && ARCoreManager.instance.instancedPrefab)
		{
			ARCoreManager.instance.instancedPrefab.transform.Rotate(0.0f, -rotateGesture.RotationRadiansDelta * Mathf.Rad2Deg, 0.0f);
		}
	}
}
