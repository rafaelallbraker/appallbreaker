﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationManager : MonoBehaviour 
{
	Camera refCamera;
	public Animator anim;
	public LayerMask layerMask;

	bool isOpening = true;

	// Use this for initialization
	void Start () 
	{
		refCamera = GameObject.Find("First Person Camera").GetComponent<Camera>();
		anim.enabled = true;
	}
	
	// Update is called once per frame
	void FixedUpdate () 
	{
		if (Input.touchCount > 0)
		{
			Ray ray = refCamera.ScreenPointToRay(Input.GetTouch(0).position);
			RaycastHit hit;

			if(Physics.Raycast(ray, out hit, layerMask) && !isOpening)
			{
				anim.SetBool("Open", !anim.GetBool("Open"));

				isOpening = true;
			}
		}
		else
		{
			isOpening = false;
		}
	}

	public void ChangeState()
	{

	}
}
